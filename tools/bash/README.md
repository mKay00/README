---
Title: Bash Interactive Command Interpreter (Shell)
Subtitle: The Language You Use Every Time You Open a Terminal
Query: true
---

Bash *is* a [scripting language](/lang/bash/) but --- more importantly --- it is the tool you use every time you login or open a terminal to interpret and send your commands to your [computer operating system](https://duck.com/lite?kae=t&q=computer operating system).

:::co-btw
Even though Bash has been the default Linux shell from the very beginning there is no reason you can't use it with other operating systems as [macOS](https://duck.com/lite?kae=t&q=macOS bash shell) and the [GIT SCM](https://duck.com/lite?kae=t&q=GIT SCM) project proves.
:::
