---
Title: Web Browsers
Subtitle: Picking the Best for *You*
Query: true

Summary:
  So many web browsers, which one is right for *you*? That depends on *your*
  needs, not what someone might tell you to do. Evaluate what you need and
  value from a web browser (and the Web in general) then make your own
  decision.

---

## Criteria

How you weigh these criteria will be completely different from someone else. 

* Safety
* Privacy
* Speed
* Open
* Power
* Resources
* Features
* Media
* Developer
* Scandals
* Active
* Adoption
* Portable
* Customization
* Extensions
* Terminal

## Chrome

* Biggest browser adoption of all
* Owned and operating by Google
* Does stuff *no one* can actually know
* Demands *trusting* Google to do the right thing
* Innovating for the Web
* Best Developer Tools of all (Service Workers)
* Runs on everything, but different levels

## Chromium

* Long-standing FOSS replacement for Chrome
* Indirectly controlled by Google
* Not kept as current as Chrome, however
* Base of most modern tech browser
* Embedded in Electron

## Firefox

* Obsessed with web standards
* Default browser on most Linux distros
* Created and maintains Mozilla Developer Network
* Passable Developer Tools (once had best)
* Written in Rust
* Mozilla is a *huge* backer of Rust
* Mozilla claims "not-for-profit"
* Massive ethics violation to promote Mr. Robot
* Bad run-ins with specific "bad apples"

## Iridium

## Brave

* Starts from default of *no* privacy tracking or ads.
* Promotes *direct* compensation for value of web sites.
* Unapologetic "bad apples" including the President.
* Affiliate relationships with crytpo company.
* Doubled-down on "nothing wrong" with changing URLs.
* Hid "the default" behind unclear language. "Customize URLs."
* Illegal "bugs" changing URLs to get money through affiliation.

## Safari

* Sometimes you are *required* to use it.

## Pale Moon

## Edge

## Tor Browser

## Vivaldi

## Opera

## Lynx

## W3M

## Nyxt
