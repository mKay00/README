---
Title: Tools
---

Collecting the right tools in your toolbox is essential for any occupation or creative endeavor. Use this list to help you fill yours and then keep them sharp. Like any artisan you can carry them to any job with you with the confidence you can complete any task that requires them. 

:::co-fyi
[Languages](/lang/), [key combinations](/key/) and [services](/services/) are considered separately.
:::

* [The Linux Operating System](./linux/)
* [Editors](./editors/)

## Essential Commands

* `ls`
* `pwd`
* `cd`
* `clear`
* `less`
* `cat`
* `file`
* `mkdir`
* `touch`
* `mv`
* `rm`
* `rmdir`
* [`vi`](./editors/vi/)
* `chmod`
* `which`
* `type`
* [`ssh`](./mupdf/)
* [`bash`](./bash/)
* `screen`
* [`tmux`](./tmux/)
* `lynx`
* [`curl`](./curl/)
* [`find`](./find/)
* [`git`](./git/)
* [`jq`](./jq/)
* [`mupdf`](./mupdf/)
* [`pandoc`](./pandoc/)
* `make`
* `perl`

*As tools are documented and reviewed they will added and linked here.*

## Good Extra Commands

* [`vim`](./editors/vim/)
* [`ed`](./editors/ed/)
* `tr`
* `base64`
* `xxd`
* `gcc`

## Legacy Commands

* `sed`
* `awk`
* `cut` 
