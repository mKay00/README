---
Title: TMUX Tasks
Query: true
---

* List All Sessions
* Attach and Resume Last Session
* Detach from Running Session
