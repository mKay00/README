---
Title: Git Version Management System
Subtitle: World's Leading Tool for Managing and Maintaining Source Code
Query: true
---

Git is a tool for storing your source code and keeping track of changes made to it otherwise known as a [version management or control system](https://duck.com/lite?q=version management or control system). Git started in 2005 because [Linus Torvaldz](https://duck.com/lite?q=Linus Torvaldz) hated the exiting options so he made his own for the [Linux kernel](https://duck.com/lite?q=Linux kernel). The difference between Git and everything else would end up propelling Git well above every other tool and system ever created. Every single Git code project repository, or "repo," is an identical copy of everyone else's repos. At any moment coders should point their Git tools to other remote repos as the master origin and start using it as the parent. This decentralized approach blew everything else away because it provides the most sustainable, easy-to-use solution to what is otherwise a very complicated problem of coordinating contributions from potentially hundreds of developers.

[Git hosting services](https://duck.com/lite?q=Git hosting services) like [GitLab](/services/gitlab/) and [GitHub](/services/github/) popped up to further support the Git approach and provide essentially a social media envelope to help with issue tracking, collaboration, and merge requests.
