---
Title: Tour of Go Workthrough
Subtitle: Helping Beginners Get Through It
---

The [Tour of Go](https://tour.golang.org) is unfortunately one of the
only beginner resources for Go even though it isn't written for beginner
programmers at all. Most notably it assumes some pretty significant
knowledge of C programming including bitwise operations. This
workthrough is designed to help absolute beginners get through it even
without any other programming experience at all. We stop and explain the
stuff that is otherwise assumed you already know.

## Exercise: Loops and Functions

<https://tour.golang.org/flowcontrol/8>

At first you just 

```go
func Sqrt(x float64) float64 {
	z := 1.0
	for i := 1; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
		fmt.Println(z)
	}
	return z
}
```

Then you can keep track of the difference:

```go
func Sqrt(x float64) float64 {
	last, z := 1.0, 1.0
	for i := 1; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
		delta := z - last
		fmt.Printf("z=%v delta=%v\n", z, delta)
		last = z
	}
	return z
}
```

And finally remove the limit of 10 and checking for a specific delta
(difference) from the last value.

```go
package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) float64 {
	last, delta, z := 1.0, 1.0, 1.0
	for delta >1e-6{
		z -= (z*z - x) / (2 * z)
		delta = math.Abs(last -z)
		//fmt.Printf("z=%v delta=%v\n", z, delta)
		last = z
	}
	return z
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(math.Sqrt(2))
}
```

It looks like Go must use the value `1e-6` internally within the
`math.Sqrt()` function.
