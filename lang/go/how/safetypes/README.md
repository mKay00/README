---
Title: Concurrency Safe Data Types in Golang
Subtitle: Keep Calm and Learn to Be Safe
Query: true

Summary:
  For the most part Go is safe for concurrency without much extra
  thought. But there are a few *easy* things to do to make sure you
  don't trip on one of the few hidden ways to write unsafe code in Go.

---

## Primitives are Safe

### Numbers

### Bytes

### Runes

### Strings

## Slices are *Not* Safe

## Maps are *Not* Safe

