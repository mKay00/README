---
Title: Common Programming Language Tasks
Subtitle: Stuff You Usually Need to Learn to Be Productive in *Any* Language
Query: true
Type: Outline
Summary:
  Most languages all support these tasks, which is why learning a few langauges generally means you can eventually learn any language, which is also why companies generally hire *developers* as opposed to specific language programmers. Most don't want to hire a [one-trick pony](/what/onetrick/). This ever growing list of tasks can guide you as you assess your own ability to code in *any* language.
Outline:
  Make Script
  Compile Program
  Use Variables
    Declare Variable
    Assign Value to Variable
    Assign a Default Variable Value
  Use Comments
    Comment Out a Single Line
    Comment Out Multiple Lines
  Use Arguments
    Read Command Arguments
  Read Input
    Read One Line of Input
    Read Many Lines of Input
  Print Output
  Use Logic
    Check a Condition
    Check for Empty String
    Check for Full String
---


