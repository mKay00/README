---
Title: Head First Approach
---

The *head first approach* was created by O'Reilly publishing as an evidence-based approach to technical learning with the maximum retention. What might seem silly at first is by design because the sillier the more likely you will remember it. This approach works and is the base strategy of most memory competition winners who create a colorful, creative journey in their minds to memorize otherwise flat, complex facts and numbers.
