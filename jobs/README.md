---
Title: Target Occupations
Subtitle: Jobs You Can Get
Query: true
---

:::co-pwz
Maybe you want to read a bit about [getting a job](./getajob/) before continuing. Most people have a lot of ambiguity about it. Some have downright wrong ideas.
:::

When deciding what topics to include for content and courses here on [RWX]{.spy} the following target occupations have been prioritized. These include the fastest growing tech careers according to the [United States Bureau of Labor and Statistics](https://www.bls.gov/ooh/computer-and-information-technology/home.htm):

 Title                                                      Growth   Salary        Schooling   Years
---------------------------------------------------------  -------- -----------  ------------ --------
  [Security Analyst](./secanalyst/)                           32%     \$99,730     Bachelors      4
  [Software Developer](./dev/)                                21%    \$105,950     Bachelors      4
  [Computer and Information Research Scientist](./sci/)       16%    \$122,840      Masters       6 
  [Web Developer](./webdev)                                   13%     \$73,760     Associates     2
  [System Administrator](./sysadmin/)                          5%     \$83,510     Bachelors      4 
  [Site Reliability Engineer](./sre/)                         ???       ???        Bachelors      4
  [IoT Embedded Developer](./iotdev/)                         ???       ???        Bachelors      4

Note that the salaries are *median* amounts and are often *far* under what can be found by [searching through job postings](https://indeed.com/).

:::co-btw
Contributors please keep the above list in mind when considering ideas for additional content. If you cannot confidently answer yes to the question "Does this content apply to every one of these occupations?" then it is likely secondary. While secondary content is fine, priority attention should be given to missing priority content first. For example, learning the React JavaScript framework is too specific and only applies to Web Developer. JavaScript and the DOM, however, are definitely a priority.

Incidently, learning web tech (HTML, CSS, JavaScript, DOM, HTTP) is required to become a cybersecurity professional since the web is second only to phishing scams as a hacker attack vector. This is why WGU requires certification as a "Web Site Developer" for their [Cybersecurity and Information Assurance Bachelor's degree program](https://www.wgu.edu/online-it-degrees/cybersecurity-information-assurance-bachelors-program.html)). It goes without saying that deep web skills are also required to win most bug bounties.
:::

## See Also

* [Indeed: Job Searching](https://indeed.com/)
* [Verified Information on Tech Salaries](https://www.levels.fyi/)
