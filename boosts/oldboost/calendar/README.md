---
Title: Annual Calendar
---

 Start         Weeks   Content
------------- -------  -------------------------------------
 May  4, 2020   16      **Linux Beginner Boost**
 Aug 23, 2020    1      (Break)
 Aug 30, 2020   16      **Linux Beginner Boost**
 Dec 21, 2020    2      (Break)
 Jan  5, 2021   16      **Linux Beginner Boost**
 Apr 26, 2021    1      (Break)
 May  3, 2021   16      **Linux Beginner Boost**

<style>
pre {
  text-align: center;
}
pre code {
  display: inline-block;
  text-align: left;
  border: none;
  max-width: none;
  width: 400px;
}
h1 {
  text-align: center;
}
</style>

              May 2020                 June 2020      
        Su Mo Tu We Th Fr Sa      Su Mo Tu We Th Fr Sa
                               #5     1  2  3  4  5  6
     #1     4  5  6  7  8  9   #6  7  8  9 10 11 12 13
     #2 10 11 12 13 14 15 16   #7 14 15 16 17 18 19 20
     #3 17 18 19 20 21 22 23   #8 21 22 23 24 25 26 27
     #4 24 25 26 27 28 29 30   #9 28 29 30
     #5 31
               July                 August       
        Su Mo Tu We Th Fr Sa      Su Mo Tu We Th Fr Sa
     #9           1  2  3  4  #13                    1
    #10  5  6  7  8  9 10 11  #14  2  3  4  5  6  7  8
    #11 12 13 14 15 16 17 18  #15  9 10 11 12 13 14 15
    #12 19 20 21 22 23 24 25  #16 16 17 18 19 20 21 22
    #13 26 27 28 29 30 31


