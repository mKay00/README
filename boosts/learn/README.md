---
Title: Learn to Learn
Subtitle: Most Important Skill You Will Every Learn
Query: true
---

## Preferences of an Autodidact

Likes:

* Reading
* Researching
* Writing
* Challenges
* Getting Started Right
* Context
* "Here's *how* it works."

Dislikes:

* Being robbed of chance to learn themselves
  * Looking in the back of the book
  * Being told the answers
  * Unchallenging HowTos and Walkthroughs
* Waiting
  * Search tools
  * People
* "It just works."


