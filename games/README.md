---
Title: Games That Make You Learn
Query: true
---

Here's a collection of games and other gamified learning resources to keep things fun. Some have walkthroughs included to help you when you get stuck.

:::co-tip
Sometimes the most entertaining game is a project that *you* want to make from scratch. You can even just look at someone else's code and modify your own.
:::

## Walkthroughs

* [PicoCTF](./picoctf/2019/)

## See Also

* [OverTheWire.org](https://overthewire.org)
* [HackTheBox.eu](https://hackthebox.eu)
* [TryHackMe.com](https://tryhackme.com)
* [CodeCombat.com](https://codecombat.com)
* [Shenzhen I/O](http://www.zachtronics.com/shenzhen-io/)
* [TIS-100](http://www.zachtronics.com/tis-100/)
* <https://zachtronics.com>
* <https://exercism.io>
* <https://leetcode.com>

