---
Title: Remote Isn't the *New* Normal, It Always Was
Subtitle: Forget About Crusty 'Ol Google, Look at GitLab
Query: true
---

Everyone's talking about how COVID is creating a "new normal" in work-from-home business models, but the fact is working remotely has *been* normal for more than a decade for several well-established companies such as IBM and newer ones like GitLab. Forget about crusty 'ol broken business models like Google. Look at modern companies like GitLab whose innovated business approaches have made it the subject of a [study by the Harvard Business Review.](https://store.hbr.org/product/gitlab-and-the-future-of-all-remote-work-a/620066) Companies and individuals unable to create value from those working remotely are increasingly paying a very real price in available talent and work-life balance.

The same goes for *most* tech education. Those learning in other cities, states and countries from their professors and mentors. This provides additional access to people in non-metropolitan areas or even in other countries. The quality of education is absolutely the same even if certain people prefer being face-to-face.

In fact, for many educational organizations like [WGU](https://duck.com/lite?kae=t&q=WGU) and [SkilStak](https://skilstak.io){.spy} it is core value and priority that people learn to get past requiring face-to-face contact in order to manage their own learning as an [autodidact](/what/autodidact/). This is why educators have a fundamental responsibility to help those in their charge practice working on things remotely. 

While many may disagree with this priority and value it remains an object reality more now than ever. Best get used to it. Working and learning remotely is not just the new normal. It *is* normal.

