---
Title: Tasks, HowTos, Walkthroughs, Recipies
Query: true

Type: index
Index:
- Title: Install Default Java JRE/JDK on Linux
  ID: lang/java/tasks/install
- Title: Install Deno for JavaScript/TypeScript Development
  ID: lang/js/deno/tasks/install
- Title: Install NodeJS Securely
  ID: lang/js/node/tasks/install
- Title: Set Up PaperMC Server on Linux
  ID: services/mc/paper/tasks/setup
- Title: Install a Minecraft Server Plugin
  ID: services/mc/tasks/plugin
- Title: Start Minecraft Server
  ID: services/mc/tasks/start
- Title: Upload a Minecraft World
  ID: services/mc/tasks/world
- Title: Authorize a Remote Public Key
  ID: services/sshd/tasks/authpub
- Title: Set Up Secure Shell Server Daemon
  ID: services/sshd/tasks/setup
- Title: Download a File with Curl
  ID: tools/curl/tasks/download
- Title: Integrate Shell Commands Into Vi Workflow
  ID: tools/editors/vi/how/magic
- Title: Install `jq`, The JSON Command Line Processing Tool
  ID: tools/jq/tasks/install
- Title: Set Up a Linux Server
  ID: tools/linux/server/tasks/setup
- Title: Set Up a `dotfiles` Configuration Project Repo
  ID: tools/linux/tasks/dotfiles
- Title: Set Up Bash Dotfiles
  ID: tools/linux/tasks/dotfiles/bash
- Title: Set Up TMUX Dotfiles Configuration Subdirectory
  ID: tools/linux/tasks/dotfiles/tmux
- Title: Set Up Vi/m Dotfiles Configuration Subdirectory
  ID: tools/linux/tasks/dotfiles/vim
- Title: Show Your Public Secure Shell (SSH) Key with `cat`
  ID: tools/ssh/tasks/catpub
- Title: Creating Secure Shell Keys
  ID: tools/ssh/tasks/keygen
- Title: Build an AUR Package
  ID: tools/linux/distros/arch/tasks/pkgbuild
- Title: Easy Bash Programmable Tab Completion
  ID: lang/bash/how/complete
- Title: Survive Vi
  ID: tools/editors/vi/how/survive
- Title: Set Up Docker
  ID: tools/docker/tasks/setup

---

