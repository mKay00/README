---
Title: How is Education Really Just Programming?
Subtitle: The Science and Process of Programming Human Brains
Query: true
---

Education is literally the process of programming human brains, coding them with the skills to be able to perform certain tasks and understand concepts comprised of terms, definitions, and ideas. How we accomplish the programming of human brains is very different.

[Machine learning](https://duck.com/lite?kae=t&q=Machine learning) isn't *machine* learning at all. It is actually based on how *humans* learn, through trial and error with some hints to help them out. Obviously that's an oversimplification but the parallels are very real.

[Algorithmic learning](https://duck.com/lite?kae=t&q=Algorithmic learning) is when the [neurons](https://duck.com/lite?kae=t&q=neurons) of the brain are changed through repetition and [mnemonic](https://duck.com/lite?kae=t&q=mnemonic) devices to contain newly programmed pathways that enable performing the task or explain a concept. 

