---
Title: What is Continuous Integration and Delivery / CICD?
Subtitle: A Way to Constantly Release Something
Query: true
---

CI/CD refers to the combined practices of continuous integration and continuous delivery. By automating the quality assurance testing changes can be made instantly and released on a [rotational](/what/rotating) schedule allowing them to be usable sooner. This concept is popular in the technology industry but has made its way into other fast-moving industries as well including [knowledge management](/what/knowledge/apps/).
