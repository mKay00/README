---
Title: Why is Saying GNU/Linux Disrespectful?
Subtitle: It Discounts Others Contributions
Query: true
---

Initially it makes sense to call *Linux* what Stallman demands. But he's wrong. So many people have contributed to everything contained in modern Linux that it has become disrespectful to single out *just* the GNU contributions. 

:::co-mad
But honestly, [GNU is dead](/views/gnuisdead/) --- or dying.
:::
