---
Title: What is Platform as a Service / PaaS?
Subtitle: Providing a Place to Run Your App in the Cloud
Query: true
---

When you use a service to host your application you are using *[platform](/what/platform/) as a service*. Google App Engine is one example of such a service.

## See Also

* [Cloud Services](../)
