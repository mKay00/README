---
Title: What is Minimum Viable Knowledge Base?
Subtitle: Consumable, Composable, Sustainable, Shareable
Query: true
---

A *minimum viable knowledge base* is a [knowledge base](/what/knowledge/base/) that follows the "do one thing well" philosophy. While the MVKB might contain thousands of knowledge nodes it makes sense as a single entity which itself can be composed into other knowledge bases. 

The *minimum viable* adjectives refer to the simplest possible tech requirement possible, which almost always means some form of Markdown --- [not a wiki](/what/wikis/) --- in a [Git repo](/what/knowledge/git/).  The simple [README](/what/knowledge/readme/) can then be shared with the [world](https://readme.world/) without much fuss at all.

