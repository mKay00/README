---
Title: What is a Mnemonic Mini-Project?
Subtitle: A Memorable Way to Practice Your Skills
---

A *mnemonic mini-project* is a small project focused only on one or two skills and concepts that employs silliness and memes to make it easier to remember and recall later. Scientific studies have proven that the more crazy or silly the activity the more memorable it is. This is the basis for the [O'Reilly Head First](/reviews/books/hf/) approach. 

:::co-faq

## Aren't you just talking about *exercises*?

Kinda, yes. But an MMP is usually a bit more involved than just a simple exercise. Most MMPs are equivalent to a few exercises that are combined together.

:::
