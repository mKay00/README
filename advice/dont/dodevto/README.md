---
Title: Don't Read and Blog on Dev.to
Subtitle: Site So Bad It Requires JavaScript to View
---

Dev.to is among the *worst* web sites for tech information on the whole web.  It can't even be viewed by any browser that doesn't have JavaScript enabled. Think about that. A technology *forum* site --- that specializes in posts about script-kiddy web development --- that depends on JavaScript to be viewed. That's how completely inexperienced and incompetent their technologists and designers are. You really do *not* want anything to do with it.

## Bad Centralized Knowledge Platform Lock-In

It's just a matter of time before Dev.to throws up a paywall (like Medium) or a bunch of ads. The entire premise of requiring a centralized platform for knowledge exchange is exactly the *opposite* of what the world needs. Instead of promoting an unsustainable dependency on a web site *that doesn't even comply with minimal web standards* and will likely eat all your posts never to give them back people should practice sustainable [knowledge](/what/knowledge/) management practices that promote a *de*-centralized, robust network of individuals sharing what they know. (And no, it doesn't have to be RWX.gg. *Anything* would be better than Dev.to. Hell, make your own site. It really isn't that hard.)

