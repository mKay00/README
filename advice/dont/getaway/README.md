---
Title: Don't Try to Get Away with Stuff
Subtitle: What if everybody did it?
Query: true
---

The best way to demonstrate your selfishness, cluelessness, and lack of
empathy --- angering and annoying people in the process destroying
future opportunities --- is to do *anything* without asking yourself,
"What if everyone did what I'm about to do?" If you don't, you are
essentially asking instead, "What can I get away with?" These
emotionally stupid people actually think that getting away with
something (not paying your taxes, stealing, littering, etc.) makes them
clever somehow. In fact, every con, hacker, and heist movie ever made
romanticizes this idea --- to a fault.

Getting away with stuff is the basis of a failed society. It is the
foundation of an anti-democracy, of anarchy (another thing romanticized
a lot in movies). Every single societal problem can be traced to someone
--- usually with money and power --- practicing the "what can I get away
with" philosophy. Whether you are [lobbying the government to make Pizza
into a fruit](https://duck.com/lite?kd=-1&kp=-1&q=lobbying the government to
make Pizza into a fruit) so you can sell more of them or you are asking
for an extra slice of Pizza at school lunch and manipulating those
handing it out you are a member of this societal scum that has
ultimately brought down that greatest civilizations on Earth without
ever realizing it, blinded by their intense narcissism and (usually
religious or economic) entitlement. These dumb-ass cancer cells for
human beings blissfully gobble up everything they can get away with like
the inflammation that causes people's feet fall off from diabetes.
Ultimately the cancer dies with the rest of the diseased society but the
cancer never realizes it was the main cause.

The opposite of this, those with [emotional
intelligence](https://duck.com/lite?kae=t&q=emotional intelligence) seek
always to check their actions --- particularly involving others --- to
make sure what they do, think, and say would be good if everyone did it.
Sometimes they realize that not everyone *could* do it, but for rational
reasons. (Not everyone should be a fire-fighter.) But usually this makes
the proper action very clear. (No one should litter, ever.)

Here's an example of one such individual with a background
that, if you knew it, would make it you go "Ohhhh, I see.":

!["What can I get away with?"](young-entitled-asshole.png)

This relatively innocent cancer cell has no idea it is destroying
society. It just wants help with an academic emergency that any kind
person would clearly help them with if they just ask nice enough. Surely
anyone saying no to such a request is the asshole, not them. They just
want to get away with as much as they can, and do the absolute minimum,
no research into an obvious way to find my email, no checking in on the
stream on Twitch, not a finger lifted to help themselves. They have an
urgency that would have been averted had they been even slightly
self-aware, slightly pro-active. But they are not and don't even know or
care.  They actually think it is okay to drop out without any
explanation, not respond to any email or chat for *two years* and then
suddenly ask me to drop everything and help them with their emergency
*for free*. This is the epitome of emotional stupidity. Don't be this
person.
